$(document).ready(function(){

	var _token=$.cookie("token");
	$.post(Url+"show_full",
	{
	    token: _token
	},
	function(response, status){
		$('table thead').empty();
		$('table tbody').empty();
		$('table thead').append('<tr><th>'+'ID'+'</th><th>'+'Supplier'+'</th></tr>');


		if(response.status==200)
		{
			var len=response.data.length;
			for(i=0;i<len;i++)
			{
				$('table tbody').append('<tr><td>'+response.data[i].ID+'</td><td>'+response.data[i].SupplierName+'</td></tr>');
			}
    }

	});

});
$("#logout").click(function(){
	if($.removeCookie('token'))
	{window.location.href="../main.html";}
});
